import {ACTION_ITEMS} from '../actions/form';

const initialstate = {
    list:[]
}

export const form = (state=initialstate, action)=>{
   
        switch (action.type) {
        case ACTION_ITEMS.FETCH_ALL:
            return{
                ...state,
                list:[...action.payload]
            }
        case ACTION_ITEMS.CREATE:
            return{
                ...state,
                list:[...state.list,action.payload]
            }

            case ACTION_ITEMS.UPDATE:
                return{
                    ...state,
                    list:state.list.map(x=>x.id==action.payload.id?action.payload:x)
                }

                case ACTION_ITEMS.DELETE:
                    return{
                        ...state,
                        list:state.list.filter(x=>x.id!=action.payload)
                    }
        default:
           return state
        }
   
}; 