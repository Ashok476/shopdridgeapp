import api from './api';
export const ACTION_ITEMS = {
    CREATE : 'CREATE',
    UPDATE:'UPDATE',
    DELETE:'DELETE',
    FETCH_ALL :'FETCH_ALL'

}
const formatData = data=>({
    ...data,
    price: parseInt(data.price?data.price:0)
})

const modifieddata = id=>({
    ...id
})
export const fetchAll = ()=> dispatch => {
    
    api.shopBridge().fetchAll()
    .then(
        response => {
            console.log(response.data)
            dispatch({
                type :ACTION_ITEMS.FETCH_ALL,
                payload:response.data
               })
        }
    )
    .catch(err => console.log(err))

    //get aoi req..
   
}

export const fetchById = (id)=> dispatch => {
    
    api.shopBridge().fetchById(id)
    .then(
        response => {
            console.log(response.data)
            dispatch({
                type :ACTION_ITEMS.fetchById,
                payload:response.data
               })
        }
    )
    .catch(err => console.log(err))

    //get aoi req..
   
}

export const create=(data, onSuccess)=>dispatch=>{
console.log(data)
data=formatData(data)
console.log(data)
api.shopBridge().create(data)
.then(res=>{
    console.log(res.data)
    dispatch({
        type:ACTION_ITEMS.CREATE,
        payload:res.datas
    })
    onSuccess()
})
.catch(err=>console.log(err))
}

export const update=(id,data, onSuccess)=>dispatch=>{
    data= formatData(data)
    api.shopBridge().create(id,data)
    .then(res=>{
        dispatch({
            type:ACTION_ITEMS.UPDATE,
            payload:[id, ...data]
        })
        onSuccess()
    })
    .catch(err=>console.log(err))
    }

    export const Delete=(id,onSuccess)=>dispatch=>{
        console.log(id)
        //id= modifieddata(id)
        api.shopBridge().delete(id)
        .then(res=>{
            dispatch({
                type:ACTION_ITEMS.DELETE,
                payload:id
            })
            onSuccess()
        })
        .catch(err=>console.log(err))
        }