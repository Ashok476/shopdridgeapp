import axios from 'axios';


const baseurl = "https://localhost:44373/api/Shopcartitems/";

export default {
    
    shopBridge(url=baseurl){
        return{
            fetchAll : ()=>axios.get(url),
            fetchById : id=>axios.get(url,id),
            create: newRecord =>axios.post(url,newRecord,{
                "headers": {
                  'Content-Type': 'application/json',
                }
            }),
            update:(id,updateRecord) => axios.put(url,id,updateRecord),
            // delete :id=>axios.delete(url,{"data": {
            //     source: id
            //   }},
            //   {
            //     "headers": {
            //       'Content-Type': 'application/json',
            //     }
            // })

            delete :id=>axios.delete("https://localhost:44373/api/Shopcartitems/"+id,
              {
                "headers": {
                  'Content-Type': 'application/json',
                }
            })

        }
    }
}