import {connect} from 'react-redux';
import * as actions from '../actions/form';
import  React,{Component, useEffect,useState} from 'react';
import  { ButtonGroup, Grid,Paper,Table, TableBody, TableCell, TableContainer, TableHead, TableRow, withStyles } from '@material-ui/core';
import Deleteicon from '@material-ui/icons/Delete';
import Shopform  from '../components/Shopform';
import { Route, withRouter,useHistory,Redirect ,Router } from 'react-router';

const styles = theme=>({

    root:{
'& .MuiTableRow-head':{
    fontSize:"1.25rem"
}
    },
    paper:{
        margin:theme.spacing(2),
        padding:theme.spacing(2)
    }
})

const Shoplist = ({classes, ...props})=>{

    console.log(props)
   const[currentId, setCurrentId] = useState(0)
    const[path,navigate] = useState(null)
    
    useEffect(()=>{        
        props.fetchAllItems()
       // path=>navigate("/ItemDetails")
    },[])
    const history = useHistory();
    const ItemDetails = (id)=>{
        window.alert("Item ID" + id)
       localStorage.setItem('ItemID',id)
       console.log("localstorage value "+localStorage.getItem('ItemID'))
     
      
        //let history = useHistory();
        props.history.push({
            pathname:  "/ItemDetails",
            // state: {
            //   response: messageFromServer 
            // } 
         })
       // <Route path="/ItemDetails" Component={ItemDetails}/>
    //}
}
    const onDelete= id=>
    {
        if(window.confirm('Are you sure to delete this item?'))
        props.deleteItem(id,()=>window.alert("deleted successfully"))
    }

    
    return (
       
        <Paper className={classes.paper} elevation={3}>
            <Grid container>
        <Grid item xs={6}>
        <Shopform />
        {/* <ItemDetails/> */}
        </Grid>
        <Route path="/ItemDetails" component={ItemDetails} />
        <Grid item xs={6}>
       <TableContainer>
           <Table>
               <TableHead className={classes.root}> 
               <TableRow>
                   <TableCell> Name  </TableCell>
                   <TableCell> Description  </TableCell>
                   <TableCell> Price  </TableCell>
                   <TableCell></TableCell>
                   </TableRow>
               </TableHead>
               <TableBody>
                   {
                       props.itemList.map((record,index)=>{
                           return (<TableRow key={index} hover
                            onClick={()=>ItemDetails(record.id)}
                            >
                               <TableCell>{record.itemname}</TableCell>
                               <TableCell>{record.description}</TableCell>
                               <TableCell>{record.price}</TableCell>
                               <TableCell>
                                   <ButtonGroup variant="text">
                                       <button><Deleteicon color="secondary"
                                       onClick={()=>onDelete(record.id)}></Deleteicon></button>
                                       </ButtonGroup>
                               </TableCell>
                           </TableRow> 
                                              
                           )
                       })
                   }
               </TableBody>
           </Table>
            </TableContainer>
        </Grid>
        {/* <Grid item xl={3}>
        <ItemDetails />
        </Grid> */}
        </Grid>
        
        </Paper>
        
    );
}

const mapstatetoprops =state=>({
    itemList : state.form.list
    })

const mapActiontoprops={
    fetchAllItems : actions.fetchAll,
    deleteItem : actions.Delete,
    fetchItem : actions.fetchById
}

export default withRouter(connect(mapstatetoprops,mapActiontoprops)((withStyles(styles)(Shoplist))));

