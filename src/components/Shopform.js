import { FormControl, Grid, InputLabel,Select, MenuItem, TextField, Button } from '@material-ui/core';
import { withStyles } from '@material-ui/core/styles';
import React, { useState,Component, useEffect } from 'react';
import useForm from "./useform";
import {connect} from 'react-redux';
import * as actions from '../actions/form';
import {useToasts} from 'react-toast-notifications'


const styles = theme=>({
    root:{
        '& .MuiTextField-root':{
            margin:theme.spacing(1),
            minwidth:230
        }
    },
    formControl:{
        '& .MuiTextField-root':{
            margin:theme.spacing(1),
            minwidth:230
        }
    },
    setMargin:{
        margin:theme.spacing(1)
    }
})
const initialFieldvalues= {
    description:'',
    itemName:'',
    price:'',
    
}
const Shopform = ({classes, ...props})=>{

    //toast msg
    //const [addToast] = useToasts()

    const validate = (fieldvalues = values) =>{
        let temp={...errors}
        //if('itemName' in fieldvalues)
        temp.itemName = fieldvalues.itemName?"":"this field is required"
       // if('description' in fieldvalues)
        temp.description = fieldvalues.description ? "":"this field is required"
       // if('price' in fieldvalues)
        temp.price = fieldvalues.price ? "":"this field is required"
        setErrors({
            ...temp
        })
        if(fieldvalues==values)
        return Object.values(temp).every(x=>x=="")
    }
    
    const {values,
        setValues,
        errors,
        setErrors,
        handleInputChanges,
        resetForm
    } = useForm(initialFieldvalues,validate,props.setCurrentId)
    const handleSubmit= e=>{
        e.preventDefault(
            console.log(values)
        )
        if(validate())
        {
        //    const onSuccess = ()=>resetForm()
        //    addToast('Submitted Successfully')
       props.createitem(values,()=>window.alert("inserted successfully"))
       window.location.reload(false);
        }
    }
    

    useEffect(() => {
        if(props.currentId!=0)
        setValues({
            ...props.itemList.find(x => x.id==props.currentId)
        })
    }, [props.currentId])

    return (
        <form autoComplete="off" noValidate className={classes.root} onSubmit={handleSubmit} > 
            <Grid container>
                <Grid item xs={6}>
                    <TextField 
                    name="itemName"
                    variant="outlined"
                    label="Item Name"
                    value={values.itemName}
                    onChange={handleInputChanges}
                    {...(errors.itemName && {error:true,helperText:errors.itemName})}
                    />

                    <TextField 
                    name="description"
                    variant="outlined"
                    label="Description"
                    value={values.description}
                    onChange={handleInputChanges}
                    {...(errors.description && {error:true,helperText:errors.description})}
                    />
                   <TextField 
                    name="price"
                    variant="outlined"
                    label="Price"
                    value={values.price}
                    onChange={handleInputChanges}
                    {...(errors.price && {error:true,helperText:errors.price})}
                    />
                    
                    <div>
                        <Button
                        variant="contained"
                        color="primary"
                        type="submit">
                            Submit
                        </Button>
                        <Button
                        variant="contained"
                         className={classes.setMargin}
                         onClick={resetForm}
                        >
                            Reset
                        </Button>
                    </div>
                </Grid>
            </Grid>
        </form>
    );
}

const mapstatetoprops =state=>({
    itemList : state.form.list
    })

const mapActiontoprops={
    createitem : actions.create,
    updateitem: actions.update
}

export default connect(mapstatetoprops,mapActiontoprops) (withStyles(styles)(Shopform));