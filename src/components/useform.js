import React, { useState ,useEffect} from 'react';

const useForm =(initialFieldvalues,validate,setCurrentId)=>{
const [values,setValues] = useState(initialFieldvalues)
const [errors,setErrors] =useState({})

const handleInputChanges = e=>{
    const{name,value} = e.target
    const fieldvalue = {[name] : value}
    setValues({
        ...values,
       ...fieldvalue
    })
    validate(fieldvalue)
}

const resetForm =()=>{
    setValues ({
        ...initialFieldvalues
    })
    setErrors({})
    setCurrentId()

}

return{
    values,
    setValues,
    handleInputChanges,
    errors,
    setErrors,
    resetForm,setCurrentId
};
}

export default useForm;