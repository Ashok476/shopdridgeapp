import './App.css';
import  {store } from './actions/store';
import {Provider} from 'react-redux';
import Shoplist from './components/Shoplist';
import { Container } from '@material-ui/core';
import ItemDetails from './components/ItemDetails';
import {Route} from 'react-dom';
import { BrowserRouter} from 'react-router-dom';
import {ToastProvider} from 'react-toast-notifications'

function App() {
  return(
    <BrowserRouter>
    <Provider store={store}>
      <ToastProvider autoDismiss="true">
      <Container maxWidth="lg">
        <Shoplist/>
        
      {/* <Route path="/ItemDetails" component={ItemDetails}/> */}
      </Container>
      </ToastProvider>    
        </Provider>
        </BrowserRouter>
  )
}

export default App;
