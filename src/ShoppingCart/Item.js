import React, { Component } from 'react';

class Item extends Component{


    constructor(props){
        super(props);
        this.state={
            itemname:'',
            itemdescription:'',
            itemprice:''
        };

       // this.handleChange = this.handleChange.bind(this);
        this.handleSubmit = this.handleSubmit.bind(this);
    }

    NameChange=(e)=>{
        this.setState({
            itemname:e.target.value
        })
    }
    DescriptionChange=(e)=>{
        this.setState({
            itemdescription:e.target.value
        })
    }
    PriceChange=(e)=>{
        this.setState({
            itemprice:e.target.value
        })
    }

    handleSubmit(event){
        alert(this.state.itemname)
        console.log(this.state)
        event.preventDefault();
    }
    
    render(){
        return(
            <form onSubmit={this.handleSubmit}>
                <label>
                    Item Name
                    <input type="text" value={this.props.itemname} onChange={this.NameChange}></input>
                </label>
                <label>
                    Item Descrition
                    <input type="text" value={this.props.itemdescription} onChange={this.DescriptionChange}></input>
                </label>
                <label>
                    Item price
                    <input type="text" value={this.props.itemprice} onChange={this.PriceChange}></input>
                </label>
                <input type="submit" value="Submit"></input>
            </form>
        )
    }
}

export default Item;