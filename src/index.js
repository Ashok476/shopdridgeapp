import React, { Component } from 'react';
import ReactDOM from 'react-dom';
import './index.css';
import App from './App';
import reportWebVitals from './reportWebVitals';
import {Route} from 'react-router';
import { BrowserRouter } from 'react-router-dom'
import ItemDetails from './components/ItemDetails';

const app = (
  <BrowserRouter>
  <React.StrictMode>
      <App >
      <Route path="/ItemDetails" component={ItemDetails}/> 
      </App>
    </React.StrictMode>
    </BrowserRouter>
)


ReactDOM.render(
  app,
  document.getElementById('root')
);

// If you want to start measuring performance in your app, pass a function
// to log results (for example: reportWebVitals(console.log))
// or send to an analytics endpoint. Learn more: https://bit.ly/CRA-vitals
reportWebVitals();
